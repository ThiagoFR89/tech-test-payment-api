using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VENDEDORController : ControllerBase
    {
       private readonly PaymentContext _context;
        public VENDEDORController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CriarVendedor")]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(VisualizarVendedorPorId), new { id = vendedor.Id }, vendedor);  
        }

        [HttpGet("VisualizarVendedorPorId")]
        public IActionResult VisualizarVendedorPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if(vendedor == null){
                return NotFound();
            }

            return Ok(vendedor);
        }

        [HttpGet("VisualizarTodosVendedores")]
        public IActionResult VisualizarVendedores()
        {
            var vendedor = _context.Vendedores;
            return Ok(vendedor);
        }
        
        [HttpDelete("DeletarVendedor")]
        public IActionResult Deletar(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if (vendedor == null){
                return NotFound();
            }
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("AtualizarVendedor")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBd = _context.Vendedores.Find(id);
            
            if (vendedorBd == null){
                return NotFound();
            }

            vendedorBd.Nome = vendedor.Nome;
            vendedorBd.Cpf = vendedor.Cpf;
            vendedorBd.Email = vendedor.Email;
            vendedorBd.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBd);
            _context.SaveChanges();
            return Ok(vendedorBd);
        }
 
    }
}