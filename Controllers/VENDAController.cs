using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VENDAController : ControllerBase
    {
        private readonly PaymentContext _context;
        public VENDAController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CriarPedido")]
        public IActionResult CriarPedido(Venda venda)
        {
            venda.Status = StatusVendas.Status.AguardandoPagamento;
            venda.Data = DateTime.Now;

            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            if (vendedor == null)
            {
                return BadRequest(new { Erro = "Vendedor não cadastrado!" });
            }

            var item = _context.Itens.Find(venda.ItemId);
            if (item == null)
            {
                return BadRequest(new { Erro = "Item não cadastrado!" });
            }

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(VisualizarPedidoPorId), new { id = venda.Id }, venda);
        }

        [HttpGet("VisualizarPedidoPorId")]
        public IActionResult VisualizarPedidoPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            var item = _context.Itens.Find(venda.ItemId);

            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPost("AtualizarStatusPedido")]
        public IActionResult Atualizar(int id, Venda pedido)
        {
            var pedidoBanco = _context.Vendas.Find(pedido.Id);

            if (pedidoBanco.Status == StatusVendas.Status.AguardandoPagamento)
            {
                if (pedido.Status == StatusVendas.Status.PagamentoAceito)
                {
                    pedidoBanco.Status = StatusVendas.Status.PagamentoAceito;
                }
                else if (pedido.Status == StatusVendas.Status.Cancelado)
                {
                    pedidoBanco.Status = StatusVendas.Status.Cancelado;
                }
                else
                {
                    return BadRequest(new { Erro = "Atualização não disponivel!" });
                }
            }
            else if (pedidoBanco.Status == StatusVendas.Status.PagamentoAceito)
            {
                if (pedido.Status == StatusVendas.Status.EnviadoParaTransportadora)
                {
                    pedidoBanco.Status = StatusVendas.Status.EnviadoParaTransportadora;
                }
                else if (pedido.Status == StatusVendas.Status.Cancelado)
                {
                    pedidoBanco.Status = StatusVendas.Status.Cancelado;
                }
                else
                {
                    return BadRequest(new { Erro = "Atualização não disponivel!" });
                }
            }
            else if (pedidoBanco.Status == StatusVendas.Status.EnviadoParaTransportadora)
            {
                if (pedido.Status == StatusVendas.Status.Entregue)
                {
                    pedidoBanco.Status = StatusVendas.Status.Entregue;
                }
                else
                {
                    return BadRequest(new { Erro = "Atualização não disponivel!" });
                }
            }
            else
            {
                return BadRequest(new { Erro = "Atualização não disponivel!" });
            }

            _context.Vendas.Update(pedidoBanco);
            _context.SaveChanges();
            return Ok(pedidoBanco);
        }
    }
}