using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ITEMController : ControllerBase
    {
    private readonly PaymentContext _context;
      public ITEMController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CriarItem")]
        public IActionResult CriarItem(Item item)
        {
            _context.Add(item);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(VisualizarItemPorId), new { id = item.Id }, item);  
        }

        [HttpGet("VisualizarItemPorId")]
        public IActionResult VisualizarItemPorId(int id)
        {
            var itemBanco = _context.Itens.Find(id);

            if(itemBanco == null){
                return NotFound();
            }

            return Ok(itemBanco);
        }

        [HttpGet("VisualizarTodosItens")]
        public IActionResult VisualizarItens()
        {
            var itemBanco = _context.Itens;
            return Ok(itemBanco);
        }

        [HttpDelete("DeletarItem")]
        public IActionResult Deletar(int id)
        {
            var itemBanco = _context.Itens.Find(id);

            if (itemBanco == null){
                return NotFound();
            }
            _context.Itens.Remove(itemBanco);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("AtualizarValorItem")]
        public IActionResult Atualizar(int id, Item item)
        {
            var itemBanco = _context.Itens.Find(id);
            
            if (itemBanco == null){
                return NotFound();
            }
            
            itemBanco.Descricao = item.Descricao;
            itemBanco.Valor = item.Valor;

            _context.Itens.Update(itemBanco);
            _context.SaveChanges();
            return Ok(itemBanco);
        }  
    }
}