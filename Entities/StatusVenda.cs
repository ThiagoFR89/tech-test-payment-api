using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public enum StatusVenda
    {
        Aguadando_Pagamento,
        Pagamento_Aprovado,
        Enviado_para_Transportadora,
        Entregue,
        Cancelado
    }
}