using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tech_test_payment_api.StatusVendas;


namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        [Key()]
        public int Id {get; set; }
        [ForeignKey("Vendedor")]
        public int VendedorId { get; set; }
        public virtual Vendedor Vendedor { get; set; }
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        public virtual Item Item { get; set; }
        public DateTime Data { get; set; }
        public Status Status { get; set; }

    }
}